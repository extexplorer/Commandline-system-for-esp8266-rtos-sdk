#ifndef tools_h__
#define tools_h__
#include "type_def.h"

count_t1 myStrCmp(byte_t* str1,byte_t* str2,int str1_size);
count_t1 myStrCopy(byte_t* from,byte_t* to);
count_t1 int2Str(int num,byte_t* str,count_t1 len);
count_t1 intPart2Str(unsigned int num,byte_t* str,count_t1 idx,count_t1* len);
count_t1 decPart2Str(unsigned int num,byte_t* str,count_t1 idx,count_t1* len,flag_t prec);
count_t1 float2Str(float num,byte_t* str,count_t1 len,char prec);
count_t1 double2Str(double num,byte_t* str,count_t1 len,char prec);
count_t1 uint2Str(unsigned int num,byte_t* str,count_t1 len);
count_t1 hex2Str(unsigned int num,byte_t* str,count_t1 len,flag_t bitc);
count_t1 deg2DMS(double deg,byte_t* str,count_t1 len);
flag_t str2Hex(char* str,char str_len,unsigned int* val);

#endif // tools_h__
