#Commandline system for esp8266 rtos sdk

一个小巧的命令行系统，类似AT命令，可以通过串口输入命令，然后执行相应程序
可以自定义命令
也可以应用于其他无操作系统和有操作系统的嵌入式系统中一获得人机交互能力
系统任务就是：接受来自外部的命令字符串--->解析命令
在用户自定义的命中，可以获得解析出来的命令参数，然后在根据参数执行其他程序
文件列表中只有sta.c和sta.h与esp8826-rtos-sdk有关，其他的都无关
主要是以下几个文件
cmd.c cmd.h tools.c tools.h type_def.h allcmd.c

先解释下命令行的组成：   
___________________________________________________    
命令行：cmd -a,a1,a2,a3 aa1 aa2 aa3 -b,b1,b2 bb1 bb2  
   
命令行组成：    
________________________   
cmd --- 命令名   
________________________    
-a,a1,a2,a3 aa1 aa2 aa3 -b,b1,b2 bb1 bb2 ---- 命令行的参数    
________________________    
a,a1,a2,a3 aa1 aa2 aa3 --- 命令行的第一个参数    
a,a1,a2,a3 ---- 第一个参数的选项    
a: --选项的第一个子选项   
a1: --选项的第二个子选项   
a2: --选项的第三个子选项   
a3: --选项的第四个子选项   
________________________   
aa1 aa2 aa3---- 第一个参数的所有子参数    
aa1: --第一个参数的第一个子参数    
aa2: --第一个参数的第二个子参数    
aa2: --第一个参数的第三个子参数    
________________________    
b,b1,b2 bb1 bb2 --- 命令行的第二个参数    
b,b1,b2 --- 第二个参数的选项    
bb1 bb2--- 第二个参数的子参数   
________________________   
以此类推......    
________________________    
命令和参数之间以 ' ' 隔开   
参数之间以 '-' 开头   
子选项之间用 ',' 隔开，不能有其他字符（包括空格）    
子参数之间以 ' ' 隔开    
子参数和子选项之间以' '隔开    
命令行以回车结尾     

ctrl+s 启用或禁用命令系统    
___________________________________________________     
命令行中参数的最大个数由cmd.h中的宏 MAX_PARA 决定    
命令行中参数的子选项的最大个数由cmd.h中的宏 MAX_SUB_OPT 决定    
命令行中参数的子参数的最大个数由cmd.h中的宏 MAX_SUB_PARA 决定    
接收命令的命令缓冲区最大大小由cmd.h中的宏 MAX_CMDL_BUF_SIZE 决定     
自定义命令的最大个数由cmd.h中的宏 MAX_INNER_CMD_COUNT 决定   
___________________________________________________    
命令系统工作方式:   

系统提供了2个函数  
1，initCmdSys()：----系统初始化函数  
2，copyCmdCharToBuf():--- 从串口中接受一个字符，可以在一个大循环中调用，也可以在中断中调用   
___________________________________________________    
如何新建一个命令:    
___________________________________________________    
1，新建一个源文件    
2，包含cmd.h文件    
3，写命令执行函数,如:aaCmd()，在命令函数执行完之前务必调用endCmd()函数    
4，在allcmd.c文件initCmdSys()中,加入语句addCmd("aa",aaCmd);aa为命令行输入的命令名    
5, OK    
___________________________________________________   
在命令执行函数中如何解析参数：    
一般形式如下：  
int i,j;   
ParaStr para;   
for(i=0;i<PARA_COUNT;i++)   
{    
    for(j=0;j<SUBOPT_COUNT(i);j++)   
    {   
        getSubOptStr(i,j,&para);   
        ....    
    }    
    for(j=0;j<SUBOPT_COUNT(i);j++)    
    {    
        getSubParaStr(i,j, &para);    
        ....    
    }    
}    
________________________     
getSubOptStr(i,j,&para) -- 获得第i个参数的第j个子选项，结果保存在para中    
getSubOptStr(i,j,&para) -- 获得第i个参数的第j个子参数，结果保存在para中   
例如开始的那个示例命令行：    
getSubOptStr(0,1,&para) -- 第0个参数的第1个子选项，即字符串 a1   
para.addr 为a1的地址   
para.len 为a1的长度，为2   
getSubParaStr(1,2,&para) -- 第1个参数的第2个子参数，即字符串 bb2    
para.addr 为bb2的地址    
para.len 为bb2的长度，为3    
___________________________________________________    

文件列表中有一个sta命令的示例代码，可以用来扫描AP和连接AP   

联系：xa7922@163.com   
