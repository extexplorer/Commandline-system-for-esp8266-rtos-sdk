#include "cmd.h"
#include "tools.h"

#include "../driver/include/uart.h"

flag_t enable_cmdsys;
byte_t cmd_recv_buff[MAX_CMDL_BUF_SIZE];
CmdLineParas cmd_para;

static CmdLine cmd_line;
static InnerCmds commands;
static InnerCmd* cur_cmd;

void  initCmdSysData()
{
	int i,j;
	for(i=0;i<MAX_CMDL_BUF_SIZE;i++)
		CMDL_BUF(i)=0;
	CMDL_NAME_OFFSET=0;
	CMDL_NAME_LEN=0;
	CMDL_PARA_COUNT=0;
	for(i=0;i<MAX_PARA;i++)
	{
		CMDL_SUBOPT_COUNT(i)=0;
		CMDL_SUBPARA_COUNT(i)=0;
		CMDL_OPT_START(i)=0;
		CMDL_OPT_END(i)=0;
		for(j=0;j<MAX_SUB_OPT;j++)
		{
			CMDL_SUBPARA_OFFSET(i,j)=0;
			CMDL_SUBOPT_LEN(i,j)=0;
		}
		for(j=0;j<MAX_SUB_PARA;j++)
		{
			CMDL_SUBPARA_OFFSET(i,j)=0;
			CMDL_SUBPARA_LEN(i,j)=0;
		}
	}
	cmd_para.para_count=0;
	for(i=0;i<MAX_PARA;i++)
	{
		cmd_para.para[i].subpara_count=0;
		cmd_para.para[i].subopt_count=0;
	}
	CMD_COUNT=0;
	cur_cmd=0;
}
void  initCmdLine()
{
	int i=0,j=0;
	for(i=0;i<MAX_PARA;i++)
	{
		CMDL_SUBOPT_COUNT(i)=0;
		CMDL_SUBPARA_COUNT(i)=0;
		SUBOPT_COUNT(i)=0;
		SUBPARA_COUNT(i)=0;
		for(j=0;j<MAX_SUB_OPT;j++)
			CMDL_SUBOPT_LEN(i, j) = 0;
		for(j=0;j<MAX_SUB_PARA;j++)
			CMDL_SUBPARA_LEN(i, j) = 0;
	}
	for(i=0;i<MAX_PARA;i++)
	{
		SUBOPT_COUNT(i)=0;
		SUBPARA_COUNT(i)=0;
	}
	CMDL_PARA_COUNT = 0;
	CMDL_NAME_LEN = 0;
	PARA_COUNT = 0;
}
InnerCmd*  findCmd()
{
	int i;
	for(i=0;i<CMD_COUNT;i++)
		if(myStrCmp(CMDL_NAME_ADDR,CMD_NAME(i),CMDL_NAME_LEN))
			return CMD(i);
	return 0;
}

void  addCmd(byte_t* name,DoCmd dof)
{
	if(CMD_COUNT>=MAX_INNER_CMD_COUNT)
		return;
	CMD_COUNT++;
	CMD_NAME(CMD_COUNT-1)=name;
	CMD_DO_FUNC(CMD_COUNT-1)=dof;
}
void  parseCmdLine()
{
	int i,j=0;
	flag_t is_new=0;
	flag_t str_type=0;
	if(CMDL_SIZE==0) return;
	for(i=0;i<CMDL_SIZE;i++)
	{
		if(CMDL_BUF(i)!=SEPARATOR&&is_new==0)
		{
			if(CMDL_BUF(i)==OPT_FLAG)
			{
				if(CMDL_NAME_LEN==0) return; 
				if(++CMDL_PARA_COUNT>MAX_PARA)
				{
					CMDL_PARA_COUNT--;
					goto Sub_Opt_Decode;
				}
				CMDL_OPT_START(CMDL_PARA_IDX)=i+1;
				str_type=2;
			}
			else
			{
				if(CMDL_NAME_LEN==0)
				{
					CMDL_NAME_OFFSET=i;
					str_type=1;
					is_new=1;
					continue;
				}
				if(CMDL_PARA_COUNT==0)return;
				if(++CMDL_SUBPARA_COUNT(CMDL_PARA_IDX)>MAX_SUB_PARA)
				{
					CMDL_SUBPARA_COUNT(CMDL_PARA_IDX)--;
					str_type=0;
					is_new=1;
					continue;
				}
				CMDL_SUBPARA_OFFSET(CMDL_PARA_IDX,CMDL_SUBPARA_IDX)=i;
				str_type=3;
			}
			is_new=1;
		}
		else if((CMDL_BUF(i)==SEPARATOR)&&is_new==1)
		{
			switch(str_type)
			{
			case 1:
				CMDL_NAME_LEN=i-CMDL_NAME_OFFSET; 
				break;
			case 2: 
				CMDL_OPT_END(CMDL_PARA_IDX)=i-1;
				break;
			case 3: 
				CMDL_SUBPARA_LEN(CMDL_PARA_IDX,CMDL_SUBPARA_IDX)= 
					i-CMDL_SUBPARA_OFFSET(CMDL_PARA_IDX,CMDL_SUBPARA_IDX);
				break;
			}
			is_new=0; 
		}
	}
	if(is_new==1)
	{
		switch(str_type)
		{
		case 1:
			CMDL_NAME_LEN=CMDL_SIZE-CMDL_NAME_OFFSET;
			break;
		case 2:
			CMDL_OPT_END(CMDL_PARA_IDX)=CMDL_SIZE-1;
			break;
		case 3:
			CMDL_SUBPARA_LEN(CMDL_PARA_IDX,CMDL_SUBPARA_IDX)=
				CMDL_SIZE-CMDL_SUBPARA_OFFSET(CMDL_PARA_IDX,CMDL_SUBPARA_IDX);
			break;
		}
	}
Sub_Opt_Decode: 
	for(i=0;i<CMDL_PARA_COUNT;i++)
	{
		is_new=0;
		for(j=CMDL_OPT_START(i);j<=CMDL_OPT_END(i);j++)
		{
			if(CMDL_BUF(j)!=SUB_SEPARATOR&&is_new==0)
			{
				if(++CMDL_SUBOPT_COUNT(i)>MAX_SUB_OPT)
				{ 
					CMDL_SUBOPT_COUNT(i)--;
					break;
				}
				CMDL_SUBOPT_OFFSET(i,CMDL_SUBOPT_COUNT(i)-1)=j;
				is_new=1;
			}
			else if(CMDL_BUF(j)==SUB_SEPARATOR&&is_new==1)
			{
				is_new=0;
				CMDL_SUBOPT_LEN(i,CMDL_SUBOPT_COUNT(i)-1)=
					j-CMDL_SUBOPT_OFFSET(i,CMDL_SUBOPT_COUNT(i)-1);
			}
		}
		if(is_new==1) 
			CMDL_SUBOPT_LEN(i,CMDL_SUBOPT_COUNT(i)-1)=
			CMDL_OPT_END(i)-CMDL_SUBOPT_OFFSET(i,CMDL_SUBOPT_COUNT(i)-1)+1;
	}
	getCmdLinePara(); 
}

void  doCmd()
{
	cur_cmd=findCmd(); 
	if(cur_cmd==0) {endCmd();return;}
	cur_cmd->do_cmd(); 
}

void  endCmd()
{
	CMDL_SIZE=0;
	cur_cmd=0;
	NEXT_LINE;
	OUTCHAR('>');
	OUTCHAR('>');
}

void  getCmdLinePara()
{
	count_t1 i;
	cmd_para.para_count=CMDL_PARA_COUNT;
	for(i=0;i<CMDL_PARA_COUNT;i++)
	{
		cmd_para.para[i].subopt_count=CMDL_SUBOPT_COUNT(i);
		cmd_para.para[i].subpara_count=CMDL_SUBPARA_COUNT(i);
	}
}

void   getSubParaStr(count_t1 i,count_t1 j,ParaStr* str)
{
	str->addr=CMDL_SUBPARA_ADDR(i,j);
	str->len=CMDL_SUBPARA_LEN(i,j);
}

void   getSubOptStr(count_t1 i,count_t1 j,ParaStr* str)
{
	str->addr=CMDL_SUBOPT_ADDR(i,j);
	str->len=CMDL_SUBOPT_LEN(i,j);
}

char copyCmdCharToBuf(char ch)
{
	static volatile flag_t ctrl_code=0;
	if(ch>0 && ch<128)
	{
		switch(ch)
		{
		case CH_BACKSPACE:
			ctrl_code=0;
			if(CMDL_SIZE==0) break;
			OUTCHAR(CH_BACKSPACE);
			OUTCHAR(CH_SPACE);
			OUTCHAR(CH_BACKSPACE);
			CMDL_SIZE--; 
			break;
		case CH_ENTER:
			ctrl_code=0;
			if(enable_cmdsys==0) break; 
			if(cur_cmd!=0) break;
			/*initCmdLine();
			parseCmdLine();
			doCmd(); */ //esp系统中使用一个单独的任务来解析和执行命令，这里就不需要了
			NEXT_LINE; 
			return 1;
		case CH_CTRL_X:
			ctrl_code=0;
			if(cur_cmd==0) break; 
			endCmd();
			break;
		case CH_CTRL_S: 
			ctrl_code=0;
			if(enable_cmdsys==0)
			{
				enable_cmdsys=1;
				CLEAR_SCREEN; 
				CUR_GOHOME; 
				initCmdLine();
				endCmd();
			}
			else 
				enable_cmdsys=0;
			break;
		case CH_ESC: 
			ctrl_code=1; 
			break;
		default:
			if(ch<32 || enable_cmdsys == 0) {ctrl_code = 0;break;}
			if(cur_cmd != 0) break;
			if(CMDL_SIZE >= MAX_CMDL_BUF_SIZE) break; 
			CMDL_BUF(CMDL_SIZE)=ch; 
			CMDL_SIZE++; 
			if(ctrl_code>=1) 
			{
				if(ch==CH_BRACKET)
				{ctrl_code=2;break;}
				else if(ctrl_code>=2)
				{ctrl_code=0;break;}
			}
			OUTCHAR(ch);
			break;
		}
	}
	return 0;
}


