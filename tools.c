#include "tools.h"

#define MAX_INT_COUNT 5



//字符串复制,from以零结尾
count_t1 myStrCopy(byte_t* from,byte_t* to)
{
	int i=0;
	while(from[i]!=0)
	{
		to[i]=from[i];
		i++;
	}
	return i;
}
//字符串比较,str1末尾没有零,但有长度,str2末尾有零
count_t1 myStrCmp(byte_t* str1,byte_t* str2,int str1_size)
{
	int i=0;
	while(str2[i]!=0)
		i++;
	if(i!=str1_size)
		return 0;
	for(i=0;i<str1_size;i++)
		if(str1[i]!=str2[i])
			return 0;
	return 1;
}
//16进制转字符串,bitc为转化出来的16进制字符串有几位
count_t1 hex2Str(unsigned int num,byte_t* str,count_t1 len,flag_t bitc)
{
	count_t1 i;
	count_t1 tmp=0,idx=0;
	if(len==0) return 0;
	if(bitc!=8&&bitc!=16&&bitc!=32)
		return 0;
	 //把num倒过来转换,先把最高4位转为字符放到str中,接着是次高4位,以此类推
	for(i=bitc-4;i>=0;i-=4)
	{
		tmp=(num>>i)&0xf;
		if(tmp<10)
			str[idx++]=tmp+'0';
		else
			str[idx++]=tmp+55;
		if(idx>=len)
			return idx;
	}
	return idx; //返回占用的str的大小
}
//int转字符串
count_t1 int2Str(int num,byte_t* str,count_t1 len)
{
	unsigned int i=0;
	if(len<=0) return 0;
	if(num<0) //num为负数
	{
		num=-num;
		str[i++]='-';
	}
	return i+uint2Str(num,str+i,len-i); //返回占用的str的大小
}
//unsigned int转字符串
count_t1 uint2Str(unsigned int num,byte_t* str,count_t1 len)
{
	unsigned int tmp,rem,c=1;
	count_t1 i=0;
	if(len<=0) return 0;
	if(num>=0 && num <10) //0-10之间直接转
	{
		str[i++]=num+'0';
		return i;
	}
	while(1) //先得到num中数字的个数
	{
		c=(c<<3)+(c<<1);//c*=10;
		tmp=num/c;
		if(tmp<10)
			break;
	}
	if(i>=len) return i;
	str[i++]=tmp+'0'; //把num的最高位存下来
	rem=num%c;
	c/=10;
	while(c!=0&&i<len)
	{//依次转换各位
		tmp=rem/c;
		rem=rem%c;
		c/=10;
		str[i++]=tmp+'0';
	}
	//这样转化出来的字符串直接就正序的
	return i;//返回占用的str的大小
}

//float转字符串
count_t1 float2Str(float num,byte_t* str,count_t1 len,char prec)
{
	count_t1 i=0,j=0;
	float decp;
	int intp;
	if(len<5) return 0;
	if(num<0)
	{
		num=-num;
		str[i++]='-';
	}
	intp=(int)num; //得到整数部分
	decp=num-intp; //得到小数部分
	if(prec==5)//把小数部分转化成整数
		decp*=1e5;
	else if(prec==7)
		decp*=1e7;
	else
		return 0;
	if(intp>=0 && intp <10)
	{//0-10之间直接转换
		str[i++]=intp+'0';
		str[i++]='.';
		return decPart2Str((unsigned int)decp,str,i,&len,prec);
	}
	//转换整数部分
	i=intPart2Str((unsigned int)intp,str,i,&len);
	//转换小数部分
	i=decPart2Str((unsigned int)decp,str,i,&len,prec);
	//如果被用科学计数法表示了,就把小数部分最后一个数字到E之间的空白(如果有的话)填'0'
	if(str[len]=='e')
	{
		for(j=i;j<len;j++)
			str[j]='0';
		i+=2;
	}
	return i;
}

count_t1 double2Str(double num,byte_t* str,count_t1 len,char prec)
{
	count_t1 i=0,j=0;
	double decp;
	int intp;
	if(len<5) return 0;
	if(num<0)
	{
		num=-num;
		str[i++]='-';
	}
	intp=(int)num;
	decp=num-intp;
	if(prec==7)
		decp*=1e7;
	else if(prec==9)
		decp*=1e9;
	else
		return 0;
	if(intp>=0 && intp <10)
	{
		str[i++]=intp+'0';
		str[i++]='.';
		return decPart2Str((unsigned int)decp,str,i,&len,prec);
	}
	i=intPart2Str((unsigned int)intp,str,i,&len);
	i=decPart2Str((unsigned int)decp,str,i,&len,prec);
	if(str[len]=='e')
	{
		for(j=i;j<len;j++)
			str[j]='0';
		i+=2;
	}
	return i;
}
//浮点数整数部分转字符串
count_t1 intPart2Str(unsigned int num,byte_t* str,count_t1 idx,count_t1* len)
{
	count_t1 i=idx;
	flag_t e=0;
	unsigned int tmp,rem,j=1,c=1;
	while(1)
	{
		c*=10;
		tmp=num/c;
		if(tmp<10)
			break;
		j++;
	}
	str[i++]=tmp+'0';
	if(j>MAX_INT_COUNT-1)
	{//如果整数部分的数字个数超过了MAX_INT_COUNT,则用科学技术发表示,
		//在str的末尾写'E'和指数
		str[i++]='.';//小数点
		str[--*len]=j%10+'0';
		if(j>=10)//如果指数大于10
			str[--*len]=j%10+'0';
		str[--*len]='e';
		e=1;
	}
	rem=num%c;
	c/=10;
	while(c!=0&&i<*len)
	{
		tmp=rem/c;
		rem=rem%c;
		c/=10;
		str[i++]=tmp+'0';
	}
	if(!e&&i<*len)//如果没有被科学计数法表示,且str还有空间
		str[i++]='.';
	return i;
}
//浮点数小数部分转字符串
count_t1 decPart2Str(unsigned int num,byte_t* str,count_t1 idx,count_t1* str_len,flag_t prec)
{
	count_t1 i=idx,j=1,len_diff=0;
	unsigned int tmp,rem,c=1;
	count_t1 len=*str_len;
	while(1)
	{
		c*=10;
		tmp=num/c;
		if(tmp<10)
			break;
		j++;
	}
	len_diff=prec-(j+1);
	//如果要求的精度与num的长度不一样
	//比如8.00003,prec=7，则此时num=300 (8.00003*1e7)
	//此时,num个数为3 prec为7 ,他们之间差4个'0',要补上这4个'0'
	if(len_diff!=0)
	{
		for(j=0;j<len_diff;j++)
		{
			if(i>=len)
				return i;
			str[i++]='0';
		}
	}
	if(i>=len){return i;}
	str[i++]=tmp+'0';
	rem=num%c;
	c/=10;
	while(c!=0&&i<len)
	{
		tmp=rem/c;
		rem=rem%c;
		c/=10;
		str[i++]=tmp+'0';
	}
	return i;
}



count_t1 deg2DMS(double deg,byte_t* str,count_t1 len)
{
	int intp;
	count_t2 tmp_len=0;
	intp=(int)deg;
	if(len==11)
	{
		if(intp>=0)
			str[tmp_len++]='N';
		else
			{str[tmp_len++]='S';intp=-intp;}
	}
	else if(len==12)
	{
		if(intp>=0)
			str[tmp_len++]='E';
		else
			{str[tmp_len++]='W';intp=-intp;}
	}
	else
		return 0;
	tmp_len+=uint2Str(intp,str+tmp_len,3);
	str[tmp_len++]='.';
	deg-=intp;
	deg*=60;
	intp=(int)deg;
	tmp_len+=uint2Str(intp,str+tmp_len,2);
	str[tmp_len++]='.';
	deg-=intp;
	deg*=60;
	intp=(int)deg;
	tmp_len+=uint2Str(intp,str+tmp_len,2);
	return tmp_len;
}
flag_t str2Hex(char* str,char str_len,unsigned int* val)
{
	unsigned int hex_tmp,hex_val,i,j;
	hex_tmp=0;hex_val=0;
	i = 28 - (8 - str_len) * 4;

	for(j = 0; j < str_len; j++) 
	{
		if(*str>=0x30 && *str<=0x39)
			hex_tmp=*str-0x30;
		else if(*str>=0x41 && *str<=0x46)
			hex_tmp=*str-0x37;
		else if(*str>=0x61 && *str<=0x66)
			hex_tmp=*str-0x57;
		else
			return 0;
		hex_tmp <<= i;
		hex_val|=hex_tmp;
		i-=4;
		str++;
	}
	*val = hex_val;
	return 1;
}