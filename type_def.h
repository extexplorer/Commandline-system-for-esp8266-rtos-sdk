#ifndef type_def_h__
#define type_def_h__

typedef unsigned char count_t1; 
typedef unsigned char flag_t; 
typedef char byte_t;
typedef unsigned short int count_t2; 
typedef unsigned int count_t4;

//typedef char* va_list;
////将类型n按int类型对齐,比如n是char,则INTSIZEOF(n)是4
////n是float,则INTSIZEOF(n)是4,n是double,则INTSIZEOF(n)是8
//#define INTSIZEOF(n) ((sizeof(n) + sizeof(int) - 1) & ~(sizeof(int) - 1))
//#define VA_START(ap,v) (ap = (va_list)&v + INTSIZEOF(v))
//#define VA_ARG(ap,t) (*(t*)((ap += INTSIZEOF(t)) - INTSIZEOF(t)))
//#define VA_END(ap) (ap=(va_list)0)

#endif // type_def_h__
