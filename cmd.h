#ifndef cmd_h__
#define cmd_h__
#include "type_def.h"

#define OUTCHAR(c) uart_tx_one_char(UART0,c) //串口输出函数
#define INCHAR 
#define CH_ESC 0x1b //esc
#define CH_BRACKET 0x5b //'['
#define CLEAR_SCREEN OUTCHAR(CH_ESC); \
					 OUTCHAR(CH_BRACKET); \
					 OUTCHAR('2'); \
					 OUTCHAR('J')

#define CUR_GOHOME  OUTCHAR(CH_ESC); \
					OUTCHAR(CH_BRACKET); \
					OUTCHAR('H')
#define HIDE_CUR 	OUTCHAR(CH_ESC); \
					OUTCHAR(CH_BRACKET); \
					OUTCHAR('?'); \
					OUTCHAR('2'); \
					OUTCHAR('5'); \
					OUTCHAR('l')
#define SHOW_CUR 	OUTCHAR(CH_ESC); \
					OUTCHAR(CH_BRACKET); \
					OUTCHAR('?'); \
					OUTCHAR('2'); \
					OUTCHAR('5'); \
					OUTCHAR('h')
#define NEXT_LINE   OUTCHAR(CH_RETURN); \
					OUTCHAR(CH_NEWLINE)
#define DEL_TO_LINE_END 	OUTCHAR(CH_ESC); \
							OUTCHAR(CH_BRACKET); \
							OUTCHAR('K')
#define DEL_TO_LINE_START 	OUTCHAR(CH_ESC); \
							OUTCHAR(CH_BRACKET); \
							OUTCHAR('1'); \
							OUTCHAR('K')
#define CUR_GOTO(x,y)    	OUTCHAR(CH_ESC); \
							OUTCHAR(CH_BRACKET); \
							OUTCHAR('#y'); \
							OUTCHAR(';'); \
							OUTCHAR('#x'); \
							OUTCHAR('H')
#define SAVE_CUR_POS		OUTCHAR(CH_ESC); \
							OUTCHAR(CH_BRACKET); \
							OUTCHAR('s')
#define REST_CUR_POS	    OUTCHAR(CH_ESC); \
							OUTCHAR(CH_BRACKET); \
							OUTCHAR('u')
#define SEPARATOR ' '
#define SUB_SEPARATOR ','
#define OPT_FLAG '-'
#define CH_ENTER 0x0d //'\r'
#define CH_BACKSPACE 0x08
#define CH_CTRL_S 0x13
#define CH_CTRL_X 0x18
#define CH_SPACE ' '
#define CH_NEWLINE 0x0a //'\n'
#define CH_RETURN 0x0d //'\r'


#define MAX_PARA 4  //命令行中参数的最大个数
#define MAX_SUB_OPT 4 //命令行中参数的子选项的最大个数
#define MAX_SUB_PARA 4 //命令行中参数的子参数的最大个数
#define MAX_CMDL_BUF_SIZE 128 //接收命令的命令缓冲区最大大小
#define MAX_INNER_CMD_COUNT 16 //内部命令的最大个数
typedef void (*DoCmd)();
typedef struct
{
	count_t1 opt[2];
	count_t1 sub_opt[MAX_SUB_OPT][2]; 
	count_t1 sub_para[MAX_SUB_PARA][2];
	count_t1 sub_opt_count;	
	count_t1 sub_para_count;
}Para; 
typedef struct
{
	count_t1 name[2]; 
	count_t1 para_count; 
	Para para[MAX_PARA];
}Cmd; 
typedef struct
{
	count_t2 cmd_size; 
	Cmd cmd; 
	byte_t cmd_buf[MAX_CMDL_BUF_SIZE]; 
}CmdLine; 

typedef struct
{
	count_t1 subopt_count;
	count_t1 subpara_count;
}CmdLinePara;
typedef struct
{
	count_t1 para_count; 
	CmdLinePara para[MAX_PARA];
}CmdLineParas;

typedef struct
{
	byte_t* addr; 
	count_t1 len; 
}ParaStr;

typedef struct
{
	byte_t* name; 
	DoCmd do_cmd; 
}InnerCmd; 
typedef struct
{
	count_t1 cmd_count;
	InnerCmd cmds[MAX_INNER_CMD_COUNT];
}InnerCmds; 

#define CMDL_SIZE (cmd_line.cmd_size)
#define CMDL_BUF(m) (cmd_line.cmd_buf[m])

#define CMDL_NAME_OFFSET (cmd_line.cmd.name[0])
#define CMDL_NAME_LEN (cmd_line.cmd.name[1])
#define CMDL_NAME_ADDR (cmd_line.cmd_buf+CMDL_NAME_OFFSET)
#define CMDL_PARA_COUNT (cmd_line.cmd.para_count)

#define CMDL_PARA(n) (cmd_line.cmd.para[n])
#define CMDL_OPT_START(m) (CMDL_PARA(m).opt[0])
#define CMDL_OPT_END(m) (CMDL_PARA(m).opt[1])
#define CMDL_SUBPARA_OFFSET(m,n) (CMDL_PARA(m).sub_para[n][0])
#define CMDL_SUBPARA_LEN(m,n) (CMDL_PARA(m).sub_para[n][1])
#define CMDL_SUBPARA_ADDR(m,n) (cmd_line.cmd_buf+CMDL_SUBPARA_OFFSET(m,n))
#define CMDL_SUBOPT_OFFSET(m,n) (CMDL_PARA(m).sub_opt[n][0])
#define CMDL_SUBOPT_LEN(m,n) (CMDL_PARA(m).sub_opt[n][1])
#define CMDL_SUBOPT_ADDR(m,n) (cmd_line.cmd_buf+CMDL_SUBOPT_OFFSET(m,n))

#define CMDL_SUBPARA_COUNT(n) (cmd_line.cmd.para[n].sub_para_count)
#define CMDL_SUBOPT_COUNT(n) (cmd_line.cmd.para[n].sub_opt_count)
#define CMDL_PARA_IDX (CMDL_PARA_COUNT-1)
#define CMDL_SUBPARA_IDX (CMDL_SUBPARA_COUNT(CMDL_PARA_IDX)-1)
#define CMDL_SUBOPT_IDX (CMDL_SUBOPT_COUNT(CMDL_PARA_IDX)-1)

#define CMD_NAME(m) (commands.cmds[m].name)
#define CMD_COUNT (commands.cmd_count)
#define CMD_DO_FUNC(m) (commands.cmds[m].do_cmd)
#define CMD_UPD_FUNC(m) (commands.cmds[m].update)
#define CMD_END_FUNC(m) (commands.cmds[m].end_cmd)
#define CMD(m) (commands.cmds+(m))

#define SUBOPT_COUNT(m) (cmd_para.para[m].subopt_count)
#define SUBPARA_COUNT(m) (cmd_para.para[m].subpara_count)
#define PARA_COUNT (cmd_para.para_count)


void initCmdSys();
void initCmdSysData();
void getCmdLinePara();
void getSubParaStr(count_t1 i,count_t1 j,ParaStr* str);
void getSubOptStr(count_t1 i,count_t1 j,ParaStr* str);
void addCmd(byte_t* name,DoCmd dof);
void endCmd();
char copyCmdCharToBuf(char ch);
InnerCmd* findCmd();
void initCmdLine();
void parseCmdLine();
void getCmdLinePara();

extern CmdLineParas cmd_para;
extern flag_t enable_cmdsys;

#endif // cmd_h__


