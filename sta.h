#ifndef sta_h__
#define sta_h__

// sta -lap[,d]
//    list AP,d--for detail,default no d
// sta -jap ssid passwd
//    connect to a AP
// sta -qap
//    disconnect from a AP

#include "cmd.h"
#include "../driver/include/uart.h"
#include "esp_common.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/semphr.h"

#define STA_OPT_DETAIL "d"
#define STA_OPT_LAP "lap"
#define STA_OPT_JAP "jap"
#define STA_OPT_QAP "qap"

#define BIT_TEST(bits,bit) ((bit) == ((bits) & (bit)))

void initStaCmd();
void staCmd();
static void scan_done_op_detail(void* arg,STATUS status);
static void scan_done_op_simple(void* arg,STATUS status);
static void scan_ap(scan_done_cb_t cb);
static void copyAPInfo(struct bss_info* info);
static void joinAP(int8* ssid,count_t1 ssid_len,int8* passwd,count_t1 passwd_len);
static void sta_wifi_event_handler(System_Event_t *evt);
void  cmdTask(void* para);
void uart0_rx_intr_handler_new(void *para);

#endif